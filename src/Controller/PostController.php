<?php

namespace App\Controller;

use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PostController
 * @package App\Controller
 *
 * @Annotations\NamePrefix("post")
 *
 * @Route("/posts")
 */
class PostController extends FOSRestController
{
    /**
     * @Annotations\View(serializerGroups={"list"})
     * @Annotations\Get("/{page}", defaults={"page"=1}, requirements={"page"="\d+"})
     */
    public function listAction($page)
    {
        $query = $this->getDoctrine()->getRepository('App:Post')->getAllPublishedPosts();

        $pagerfanta = new Pagerfanta(new DoctrineORMAdapter($query, true, false));

        $pagerfanta->setMaxPerPage(10)->setCurrentPage($page);

        $posts = [];
        foreach ($pagerfanta->getCurrentPageResults() as $result) {
            $posts[] = $result;
        }

        if ($posts == null) {
            throw $this->createNotFoundException();
        }

        return [
            'total' => $pagerfanta->getNbResults(),
            'per_page' => 10,
            'posts' => $posts
        ];
    }


    /**
     * @Annotations\View(serializerGroups={"details"})
     * @Annotations\Get("/show/{id}", requirements={"id"="\d+"})
     */
    public function showAction($id)
    {
        $post = $this->getDoctrine()->getRepository('App:Post')->getPublishedPost($id);

        if ($post == null) {
            throw $this->createNotFoundException();
        }

        return $post;
    }
}
