<?php

namespace App\DataFixtures\ORM;

use App\Entity\PostTag;
use Faker;
use App\Entity\PostCategory;
use App\Entity\Post;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class PostFixture extends Fixture implements FixtureInterface
{
    const num_post = 5000;
    const num_category = 25;
    const num_tag = 25;

    public function load(ObjectManager $manager)
    {
        $faker = new Faker\Generator();
        $faker->addProvider(new Faker\Provider\Base($faker));
        $faker->addProvider(new Faker\Provider\Lorem($faker));
        $faker->addProvider(new Faker\Provider\DateTime($faker));

        for ($i = 0; $i < $this::num_category; $i++) {
            $category = new PostCategory();
            $category->setName(rtrim($faker->sentence(2), '.'));
            $manager->persist($category);
            $this->setReference('post_category_'.$i, $category);
        }

        for ($i = 0; $i < $this::num_tag; $i++) {
            $tag = new PostTag();
            $tag->setName($faker->word());
            $manager->persist($tag);
            $this->setReference('post_tag_' . $i, $tag);
        }

        for ($i = 0; $i < $this::num_post; $i++) {
            $post = new Post();
            $post->setTitle(rtrim($faker->sentence(5), '.'))
                ->setSummary($faker->paragraph(5))
                ->setContents($faker->paragraph(25))
                ->setPublishedAt($faker->randomElement([null, $faker->dateTimeBetween($startDate = '-1 year', $endDate = '+1 year')]))
                ->setCategory($this->getReference('post_category_'.$faker->numberBetween(0, $this::num_category-1)));


            foreach ($faker->randomElements(range(1, $this::num_tag-1), $faker->numberBetween(0, 5)) as $tag) {
                $post->addTag($this->getReference('post_tag_'.$tag));
            }

            $manager->persist($post);
        }

        $manager->flush();
    }
}
