<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class PostCategory
 * @package App\Entity
 *
 * @ORM\Entity()
 * @ORM\Table(name="post_tags")
 */
class PostTag extends AbstractCategory
{
    /**
     * @var Post[]
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Post", mappedBy="tags")
     */
    public $posts;
}
