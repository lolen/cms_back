<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Libs\Utils;
use JMS\Serializer\Annotation as JMSSerializer;

/**
 * Class AbstractCategory
 * @package App\Entity
 *
 * @ORM\MappedSuperclass()
 * @ORM\HasLifecycleCallbacks()
 *
 * @JMSSerializer\ExclusionPolicy("all")
 */
abstract class AbstractCategory
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     *
     * @JMSSerializer\Expose
     * @JMSSerializer\Groups(groups={"list", "details"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     *
     * @JMSSerializer\Expose
     * @JMSSerializer\Groups(groups={"list", "details"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     *
     * @JMSSerializer\Expose
     * @JMSSerializer\Groups(groups={"list", "details"})
     */
    private $slug;

    /**
     * @param string $slug
     *
     * @return AbstractCategory
     */
    public function setSlug($slug)
    {
        $this->slug = Utils::slugify($slug);

        return $this;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function callbacks()
    {
        $this->setSlug($this->name);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return AbstractCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
}
