<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class PostCategory
 * @package App\Entity
 *
 * @ORM\Entity()
 * @ORM\Table(name="post_categories")
 */
class PostCategory extends AbstractCategory
{
    /**
     * @var Post[]
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Post", mappedBy="category")
     */
    public $posts;
}
