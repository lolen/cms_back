<?php

namespace App\Repository;

use App\Entity\Post;
use Doctrine\ORM\Tools\Pagination\Paginator;



use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class PostRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Post::class);
    }

    private function publishedPost()
    {
        $qb = $this->createQueryBuilder('p')
            ->select('p')
            ->where('p.publishedAt is not null')
            ->andWhere('p.publishedAt < :date')
            ->setParameter(':date', new \DateTime());

        return $qb;
    }

    public function getAllPublishedPosts()
    {
        $qb = $this->publishedPost()
            ->orderBy('p.publishedAt', 'desc')
            ->getQuery();

        return $qb;
    }

    public function getPublishedPost($id)
    {
        $qb = $this->publishedPost()
            ->andWhere('p.id = :id')
            ->setParameter(':id', $id)
            ->getQuery()
            ->getOneOrNullResult();

        return $qb;
    }
}
